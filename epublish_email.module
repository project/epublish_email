<?php

/**
 * @file
 * Provides the functionality to send an ePublish edition as an email.
 *
 * @note
 * Module originally written by chris_five (drupal user 43715).
 */

/**
 * Implementation of hook_help().
 */
function epublish_email_help($section, $arg) {
  switch ($section) {
    case 'admin/modules#description':
      return t('E-Publish Email sends an edition from the E-Publish module to an email address');
  }
}

/**
 * Implementation of hook_menu().
 */
function epublish_email_menu() {
  $items['admin/epublish/email/edition'] = array(
    'title' => 'Email an edition',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('epublish_email_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer publications'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_form_alter().
 * Add a link to the edition edit form to the email form.
 */
function epublish_email_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'epublish_form_edition') {
    $form['email'] = array(
      '#type' => 'fieldset',
      '#title' => 'Email edition',
    );
    $form['email']['link'] = array(
      '#prefix' => '<div>',
      '#value' => l('Email this edition', 'admin/epublish/email/edition/'.arg(4)),
      '#suffix' => '</div>',
    );
  }
}

/**
 * Form to email edition.
 */
function epublish_email_form() {
  if (is_numeric(arg(4))) {
    // Send test email
    $form['emailediton'] = array(
      '#type' => 'fieldset',
      '#title' => t('Email Edition'),
      );
    $form['emailediton']['emailrecipient'] = array(
      '#type' => 'textfield',
      '#title' => t('Recipient Email Address'),
      '#size' => 50,
      '#maxlength' => 128,
      '#required' => TRUE,
      );
    $form['emailedition']['emailbody'] = array(
      '#type' => 'hidden',
      '#value' => epublish_email_body(arg(4)),
    );
    $form['emailedition']['eid'] = array(
      '#type' => 'hidden',
      '#value' => arg(4),//$edit->eid,
    );
    $form['emailedition']['emailsender'] = array(
      '#type' => 'hidden',
      '#value' => variable_get('site_mail', 'webmaster@uvmedia.com'),
    );
    $form['emailediton']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Email'),
    );
    $form['#redirect'] = array(
      'admin/epublish/edit/edition/' . arg(4)
    );
  }
  return $form;
}

function epublish_email_form_submit($form, &$form_state) {
  $edit = $form_state['values'];

  if (($publication = epublish_email_get_pub(arg(4))) && valid_email_address($edit['emailrecipient'])) {
    $pub_name = $publication->name;
  }
  else {
    drupal_set_message(t('Unable to send email. Please verify that the email address is valid.'));
  }

  if (mimemail($edit['emailsender'], $edit['emailrecipient'], $pub_name . ': Edition ' . $edit['eid'], $edit['emailbody'])) {
    drupal_set_message(t('The email was sent successfully.'));
  }
}

/**
*  epublish_email_body : create the email body for an edition
*/
function epublish_email_body($eid) {
  if (is_numeric($eid)) {
    $edition = epublish_get_edition($eid);
  }
  $publication = epublish_get_pub($edition->pid);
  $sid = epublish_assign_section($edition);
  $topics = epublish_section_topics($sid, $edition->eid);
  $output = theme('epublish_topics', $topics, $sid, $edition, 'email');

  return $output;
}

function epublish_email_get_pub($eid) {
  $output = '';
  $edition = epublish_get_edition($eid);
  $publication = epublish_get_pub($edition->pid);
  return $publication;
}